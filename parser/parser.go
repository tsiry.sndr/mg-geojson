package parser

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"

	"github.com/rs/xid"
)

const (
	country      = "data/mdg_admbnda_adm0_BNGRC_OCHA_20181031.json"
	regions      = "data/mdg_admbnda_adm1_BNGRC_OCHA_20181031.json"
	districts    = "data/mdg_admbnda_adm2_BNGRC_OCHA_20181031.json"
	communes     = "data/mdg_admbnda_adm3_BNGRC_OCHA_20181031.json"
	fokontany    = "data/mdg_admbnda_adm4_BNGRC_OCHA_20181031.json"
	countryOut   = "assets/country"
	regionsOut   = "assets/regions"
	districtsOut = "assets/districts"
	communesOut  = "assets/communes"
	fokontanyOut = "assets/fokontany"
)

type Properties struct {
	Adm0Pcode string `json:"ADM0_PCODE"`
	Adm0En    string `json:"ADM0_EN"`
	Adm1Pcode string `json:"ADM1_PCODE"`
	Adm1En    string `json:"ADM1_EN"`
	Adm1Type  string `json:"ADM1_TYPE"`
	Adm2Pcode string `json:"ADM2_PCODE"`
	Adm2En    string `json:"ADM2_EN"`
	Adm2Type  string `json:"ADM2_TYPE"`
	Adm3Pcode string `json:"ADM3_PCODE"`
	Adm3En    string `json:"ADM3_EN"`
	Adm3Type  string `json:"ADM3_TYPE"`
	Adm4Pcode string `json:"ADM4_PCODE"`
	Adm4En    string `json:"ADM4_EN"`
	Adm4Type  string `json:"ADM4_TYPE"`
	ProvCode  int    `json:"PROV_CODE_"`
	OldProvin string `json:"OLD_PROVIN"`
}

type Geometry struct {
	Type        string      `json:"type" msgpack:"type"`
	Coordinates interface{} `json:"coordinates" msgpack:"coordinates"`
}

type Feature struct {
	Properties Properties `json:"properties"`
	Geometry   Geometry   `json:"geometry"`
}

type GeoJSON struct {
	Features []Feature `json:"features"`
}

type Country struct {
	ID       string   `json:"id"`
	Name     string   `json:"name"`
	Code     string   `json:"code"`
	Geometry Geometry `json:"geometry"`
}

type Region struct {
	ID       string   `json:"id"`
	Name     string   `json:"name"`
	Code     string   `json:"code"`
	Province string   `json:"province"`
	Geometry Geometry `json:"geometry"`
}

type District struct {
	ID       string   `json:"id"`
	Name     string   `json:"name"`
	Code     string   `json:"code"`
	Province string   `json:"province"`
	Region   string   `json:"region,omitempty"`
	Geometry Geometry `json:"geometry"`
}

type Commune struct {
	ID       string   `json:"id"`
	Name     string   `json:"name"`
	Code     string   `json:"code"`
	Province string   `json:"province"`
	District string   `json:"district,omitempty"`
	Region   string   `json:"region,omitempty"`
	Geometry Geometry `json:"geometry"`
}

type Fokontany struct {
	ID       string   `json:"id" msgpack:"id"`
	Name     string   `json:"name" msgpack:"name"`
	Code     string   `json:"code" msgpack:"code"`
	Province string   `json:"province" msgpack:"province"`
	Commune  string   `json:"commune,omitempty" msgpack:"commune"`
	District string   `json:"district,omitempty" msgpack:"district"`
	Region   string   `json:"region,omitempty" msgpack:"region"`
	Geometry Geometry `json:"geometry" msgpack:"geometry"`
}

type Result struct {
	filename string
	GeoJSON  *GeoJSON
}

type Index struct {
	Name       string
	Collection string
	Items      []string
}

func Start() {
	countryCh := make(chan *Result)
	regionsCh := make(chan *Result)
	districtsCh := make(chan *Result)
	communesCh := make(chan *Result)
	fokontanyCh := make(chan *Result)

	go ParseGeoJSON(country, countryCh)
	go ParseGeoJSON(regions, regionsCh)
	go ParseGeoJSON(districts, districtsCh)
	go ParseGeoJSON(communes, communesCh)
	go ParseGeoJSON(fokontany, fokontanyCh)

	countryRes := <-countryCh
	regionsRes := <-regionsCh
	districtsRes := <-districtsCh
	communesRes := <-communesCh
	fokontanyRes := <-fokontanyCh

	log.Printf("%s file parsed ...", countryRes.filename)
	log.Printf("%s file parsed ...", regionsRes.filename)
	log.Printf("%s file parsed ...", districtsRes.filename)
	log.Printf("%s file parsed ...", communesRes.filename)
	log.Printf("%s file parsed ...", fokontanyRes.filename)

	ConvertToCountry(countryRes.GeoJSON)
	ConvertToRegions(regionsRes.GeoJSON)
	ConvertToDistricts(districtsRes.GeoJSON)
	ConvertToCommunes(communesRes.GeoJSON)
	ConvertToFokontany(fokontanyRes.GeoJSON)
}

func ParseGeoJSON(filename string, ch chan *Result) (*GeoJSON, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	result := new(GeoJSON)

	if err := json.Unmarshal(data, result); err != nil {
		log.Println(err)
		return nil, err
	}

	ch <- &Result{filename, result}

	return result, nil
}

func ConvertToCountry(g *GeoJSON) *Country {
	c := Country{}
	c.ID = xid.New().String()
	c.Code = g.Features[0].Properties.Adm0Pcode
	c.Name = g.Features[0].Properties.Adm0En
	c.Geometry = g.Features[0].Geometry
	content, err := json.MarshalIndent(c, "", "  ")
	if err != nil {
		log.Fatal(err)
		return nil
	}
	ioutil.WriteFile(countryOut+".json", content, 0644)
	return &c
}

func ConvertToRegions(g *GeoJSON) []Region {
	r := []Region{}
	all := []Region{}
	index := Index{
		Name: "regions",
	}
	items := []string{}
	indexes := []Index{}
	for i, item := range g.Features {
		region := Region{
			ID:       xid.New().String(),
			Name:     item.Properties.Adm1En,
			Code:     item.Properties.Adm1Pcode,
			Province: item.Properties.OldProvin,
			Geometry: item.Geometry,
		}
		re := Region{
			ID:       region.ID,
			Name:     region.Name,
			Code:     region.Code,
			Province: region.Province,
		}
		all = append(all, re)
		items = append(items, region.ID+":"+region.Name)
		r = append(r, region)
		if (i+1)%10 == 0 || i == len(g.Features)-1 {
			content, err := json.MarshalIndent(r, "", "  ")
			if err != nil {
				log.Fatal(err)
				return nil
			}
			filename := fmt.Sprintf("%s/region-%d.json", regionsOut, i/9)
			ioutil.WriteFile(filename, content, 0644)
			r = []Region{}
			index.Collection = fmt.Sprintf("region-%d.json", i/9)
			index.Items = items
			items = []string{}
			indexes = append(indexes, index)
		}
	}
	data, _ := json.MarshalIndent(indexes, "", "  ")
	ioutil.WriteFile("assets/regions-index.json", data, 0644)
	b, _ := json.Marshal(all)
	ioutil.WriteFile("assets/regions.json", b, 0644)
	return r
}

func ConvertToDistricts(g *GeoJSON) []District {
	d := []District{}
	all := []District{}
	index := Index{
		Name: "districts",
	}
	items := []string{}
	indexes := []Index{}
	for i, item := range g.Features {
		district := District{
			ID:       xid.New().String(),
			Name:     item.Properties.Adm2En,
			Code:     item.Properties.Adm2Pcode,
			Province: item.Properties.OldProvin,
			Geometry: item.Geometry,
		}
		di := District{
			ID:       district.ID,
			Name:     district.Name,
			Code:     district.Code,
			Province: district.Province,
			Region:   item.Properties.Adm1En,
		}
		all = append(all, di)
		items = append(items, district.ID+":"+district.Name)
		d = append(d, district)
		if (i+1)%10 == 0 || i == len(g.Features)-1 {
			content, err := json.MarshalIndent(d, "", "  ")
			if err != nil {
				log.Fatal(err)
				return nil
			}
			filename := fmt.Sprintf("%s/district-%d.json", districtsOut, i/9)
			ioutil.WriteFile(filename, content, 0644)
			d = []District{}
			index.Collection = fmt.Sprintf("district-%d.json", i/9)
			index.Items = items
			items = []string{}
			indexes = append(indexes, index)
		}
	}
	data, _ := json.MarshalIndent(indexes, "", "  ")
	ioutil.WriteFile("assets/districts-index.json", data, 0644)
	b, _ := json.Marshal(all)
	ioutil.WriteFile("assets/districts.json", b, 0644)
	return d
}

func ConvertToCommunes(g *GeoJSON) []Commune {
	c := []Commune{}
	all := []Commune{}
	index := Index{
		Name: "communes",
	}
	items := []string{}
	indexes := []Index{}
	for i, item := range g.Features {
		commune := Commune{
			ID:       xid.New().String(),
			Name:     item.Properties.Adm3En,
			Code:     item.Properties.Adm3Pcode,
			Province: item.Properties.OldProvin,
			Geometry: item.Geometry,
		}
		co := Commune{
			ID:       commune.ID,
			Name:     commune.Name,
			Code:     commune.Code,
			Province: commune.Province,
			District: item.Properties.Adm2En,
			Region:   item.Properties.Adm1En,
		}
		all = append(all, co)
		items = append(items, commune.ID+":"+commune.Name)
		c = append(c, commune)
		if (i+1)%100 == 0 || i == len(g.Features)-1 {
			content, err := json.MarshalIndent(c, "", "  ")
			if err != nil {
				log.Fatal(err)
				return nil
			}
			filename := fmt.Sprintf("%s/commune-%d.json", communesOut, i/99)
			ioutil.WriteFile(filename, content, 0644)
			c = []Commune{}
			index.Collection = fmt.Sprintf("commune-%d.json", i/99)
			index.Items = items
			items = []string{}
			indexes = append(indexes, index)
		}
	}
	data, _ := json.MarshalIndent(indexes, "", "  ")
	ioutil.WriteFile("assets/communes-index.json", data, 0644)
	b, _ := json.Marshal(all)
	ioutil.WriteFile("assets/communes.json", b, 0644)
	return c
}

func ConvertToFokontany(g *GeoJSON) []Fokontany {
	f := []Fokontany{}
	all := []Fokontany{}
	index := Index{
		Name: "fokontany",
	}
	items := []string{}
	indexes := []Index{}

	for i, item := range g.Features {
		fokontany := Fokontany{
			ID:       xid.New().String(),
			Name:     item.Properties.Adm4En,
			Code:     item.Properties.Adm4Pcode,
			Province: item.Properties.OldProvin,
			Geometry: item.Geometry,
		}
		fk := Fokontany{
			ID:       fokontany.ID,
			Name:     fokontany.Name,
			Code:     fokontany.Code,
			Province: fokontany.Province,
			Commune:  item.Properties.Adm3En,
			District: item.Properties.Adm2En,
			Region:   item.Properties.Adm1En,
		}
		items = append(items, fokontany.ID+":"+fokontany.Name)
		f = append(f, fokontany)
		all = append(all, fk)
		if (i+1)%100 == 0 || i == len(g.Features)-1 {
			content, err := json.MarshalIndent(f, "", "  ")
			if err != nil {
				log.Fatal(err)
				return nil
			}
			filename := fmt.Sprintf("%s/fokontany-%d.json", fokontanyOut, i/99)
			ioutil.WriteFile(filename, content, 0644)
			f = []Fokontany{}
			index.Collection = fmt.Sprintf("fokontany-%d.json", i/99)
			index.Items = items
			items = []string{}
			indexes = append(indexes, index)
		}
	}
	data, _ := json.MarshalIndent(indexes, "", "  ")
	ioutil.WriteFile("assets/fokontany-index.json", data, 0644)

	b, err := json.Marshal(all)
	if err != nil {
		panic(err)
	}
	ioutil.WriteFile("assets/fokontany.json", b, 0644)
	return f
}
