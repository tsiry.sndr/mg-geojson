build: 
	go run main.go start
extract:
	tar xvf data.tar.gz
clean:
	rm assets/country.json assets/fokontany.json assets/districts.json assets/regions.json assets/communes.json
	rm assets/fokontany-index.json assets/communes-index.json assets/districts-index.json assets/regions-index.json
	rm assets/communes/* assets/districts/* assets/fokontany/* assets/regions/*
serve:
	go run main.go serve
